import sys
sys.path.insert(0,'..')
from webdav_audio_library.audio_library import AudioLibrary

import json


# Load config from file
f = open ('config.json', "r")
options = json.loads(f.read())

al = AudioLibrary(options)

al.get_audio_files_recursive("library")
