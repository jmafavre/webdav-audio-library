# webdav-audio-library

A python library to retrieve audio files from a sound library hosted by a webdav server (for example owncloud).

## Use the test example

* copy ```examples/config.sample.json``` and rename it ```examples/config.json```
* edit authentification information on this new file
* run the example to clone your audio directory
